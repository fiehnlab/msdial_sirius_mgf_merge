#!/usr/bin/env python
#
# Assumes the user has
#   1) An aligned MS-DIAL peak ID export
#   2) MS-DIAL exported MGF files in a dedicated folder
#   3) SIRIUS result files in a dedicated folder
#
# For example, a directory structure could be:
# ./
#   ./MS-DIAL Peak ID Alignment.txt
#   ./MS-DIAL MGF/
#     ./Sample01.mgf
#     ./Sample02.mgf
#     ./Sample03.mgf
#   ./SIRIUS Output/
#     ./Sample01/
#       ./[Compound Identification Files]
#     ./Sample02/
#       ./[Compound Identification Files]
#     ./Sample03/
#       ./[Compound Identification Files]
#
# And the script would be run as
#   $ python msdial_sirius_mgf_merge.py "MS-DIAL Peak ID Alignment.txt" "MS-DIAL MGF/" "SIRIUS Output/"


import argparse
import numpy as np
import pandas as pd
import tqdm

from pathlib import Path


def load_mgf(filename):
    with open(filename) as fin:
        blocks = [x for x in fin.read().strip().split('\n\n')]
        data = {}

        for b in blocks:
            if 'Ms2ScanNumber: -1' in b:
                continue

            # Build a dictionary based on mgf metadata
            b = dict(x.split('=', 1) for x in b.splitlines() if '=' in x)

            # Construct SIRIUS-formatted title
            title = b['TITLE'].replace(':', '').replace('/', '').replace('\'', '').replace('?', '').replace('"', '')[:128]

            data[title] = {
                'name': b['TITLE'].rsplit('; ', 2)[0],
                'peakid': int(b['SCANS']),
                'mz': float(b['PEPMASS']),
                'rt': float(b['RTINMINUTES'])
            }

        return data


if __name__ == '__main__':
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('msdial_peak_table', type=Path)
    parser.add_argument('msdial_dir', type=Path)
    parser.add_argument('sirius_dir', type=Path)
    args = parser.parse_args()


    # Load MS-DIAL peak table
    excluded_columns = [
        'Post curation result', 'Fill %', 'MS/MS included', 'Ontology', 'SMILES', 'Comment',
        'Isotope tracking parent ID', 'Isotope tracking weight number',
        'Dot product', 'Reverse dot product', 'Fragment presence %',
        'S/N average', 'Spectrum reference file name', 'MS1 isotopic spectrum', 'MS/MS spectrum'
    ]

    df = pd.read_csv(args.msdial_peak_table, sep='\t', skiprows=3)
    df.set_index('Alignment ID', inplace=True)

    peak_columns = df.columns[df.columns.get_loc('MS/MS spectrum') + 1 :]

    # Restrict columns and sort data by m/z
    df.drop([c for c in excluded_columns if c in df.columns], axis=1, inplace=True)
    df.sort_values(['Average Mz', 'Average Rt(min)'], inplace=True)

    for c in peak_columns:
        df[c +' InChIKey'] = np.nan
        df[c +' InChI'] = np.nan
        df[c +' Links'] = np.nan


    # Loop over all MS-DIAL MFG files
    for mgf in args.msdial_dir.glob('*.mgf'):
        print(f'Parsing {mgf}')

        # Ensure that a SIRIUS folder corresponds to this MS-DIAL MGF file
        sirius_data = args.sirius_dir / mgf.stem

        if not mgf.stem in peak_columns:
            print('\tSample is not in MS-DIAL peak table, skipping')
            continue

        if not sirius_data.exists() or not sirius_data.is_dir():
            print(f'\tSIRIUS export directory for {mgf.stem} does not exist, skipping...')
            continue

        # Load MGF data
        print(mgf)
        mgf_data = load_mgf(mgf)

        for f in tqdm.tqdm(sirius_data.glob('*.csv')):
            assert(f.stem in mgf_data)
            results = pd.read_csv(f, sep='\t')

            if len(results) == 0:
                continue
            
            # Match results to alignment table
            best = results.iloc[0]
            msdial_peak = mgf_data[f.stem]

            match = df[df[mgf.stem] == msdial_peak['peakid']]

            if len(match) > 0:
                idx = match.index[0]
                df.loc[idx, mgf.stem +' InChIKey'] = best['inchikey2D']
                df.loc[idx, mgf.stem +' InChI'] = best['inchi']
                df.loc[idx, mgf.stem +' Links'] = best['links']
        


    # Drop peak id columns and identify all SIRIUS columns
    df.drop(peak_columns, axis=1, inplace=True)
    sirius_cols = [c for c in df.columns if any(c.startswith(x) for x in peak_columns)]

    # Export full alignment table as well as a filtered version where every row has at least one annotation
    df.to_csv(args.msdial_peak_table.name.replace('.txt', '_SIRIUS Results.txt'), sep='\t')
    df.dropna(how='all', subset=sirius_cols).to_csv(args.msdial_peak_table.name.replace('.txt', '_SIRIUS Results Filtered.txt'), sep='\t')
